#!/usr/bin/env bash

# expects ZSH to already be installed

cd $HOME
# clone dotfile git repo
git clone https://gitlab.com/ctdove/dotfiles.git
# get zshrc
cp dotfiles/.zshrc -o $HOME/.zshrc
# get other zsh files
mkdir -p $HOME/.zsh
cp -r dotfiles/zsh/* $HOME/.zsh/
# download and install oh-my-zsh
curl https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh \
  -o ohmyzsh_installer.sh
sh ohmyzsh_installer.sh
# install autocomplete
git clone https://github.com/zsh-users/zsh-autosuggestions \
  ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

source $HOME/.zshrc
